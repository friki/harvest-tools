Source: harvest-tools
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 12~),
               cython,
               python3-setuptools,
               protobuf-compiler,
               capnproto,
               libprotobuf-dev,
               libcapnp-dev,
               zlib1g-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/harvest-tools
Vcs-Git: https://salsa.debian.org/med-team/harvest-tools.git
Homepage: http://harvest.readthedocs.org/en/latest/content/harvest-tools.html

Package: harvest-tools
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: archiving and postprocessing for reference-compressed genomic multi-alignments
 HarvestTools is a utility for creating and interfacing with Gingr files,
 which are efficient archives that the Harvest Suite uses to store
 reference-compressed multi-alignments, phylogenetic trees, filtered
 variants and annotations. Though designed for use with Parsnp and Gingr,
 HarvestTools can also be used for generic conversion between standard
 bioinformatics file formats.
